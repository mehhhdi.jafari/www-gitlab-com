---
layout: handbook-page-toc
title: "E-Group Scheduling"
description: “Find out how to schedule with E-Group members with Executive Business Administrators”
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Before scheduling with an E-Group member, please be sure to reach out to their perspective EBA with meeting details.

## Scheduling for Michael McBride, Chief Revenue Officer

* All meeting requests routed through the EBA to the CRO, Cheri Holmes and are scheduled through her
* Prefers “appropriate length” conversations, so short meetings are okay on the calendar.  If a topic only needs 10 mins, book 10 mins instead of 30, etc.
* Include Meeting Agendas in invites / make sure the team knows to include this with requests for time.
* Flexible with late evening calls for Asia or Australia - check with him first.
* Add pre-emptive blocks in calendar that can be used for meetings or calls.
* Schedule three 30 minute blocks a day for work time - title “workflow”. This is time for email follow-up and general work time. If the time has to move to accommodate another meeting ensure another time is found and that it can still happen.


## Scheduling for Brian Robins, Chief Financial Officer

* Daily office hours are 9am-5pm ET, please Do Not Book meetings outside of working hours unless arranged with Sr. EBA
* Do not schedule over DNB (Do Not Book)
* Prefers “appropriate length” conversations, so short meetings are okay on the calendar. If a topic only needs 10 mins, book 10 mins instead of 30, etc.
* Schedule two 30 minute blocks a day for work time - title “DNB / workflow”. This is time for email follow-up and general work time. If the time has to move to accommodate another meeting ensure another time is found and that daily workflow is still achieved.
* Schedule one 60 minute block a day for for lunch - title “DNB / lunch”. This is time for a lunch break, follow-up and general work time. If the time has to move to accommodate another meeting ensure another time is found and that daily lunch break is still achieved.
* If you add any meetings to calendar directly, tick the box “Attendees can modify” so EBA can change if necessary.
* Please do not @ Brian for scheduling questions or requests in Slack, please reach out his Sr. EBA.
* Please do cc: @ Brian and his Sr. EBA with last minue scheduling changes in Slack.
* All meeting requests should include an agenda or some sort of reference material.
* During Zoom meetings please do not post information or questions in the chat, add it to the meeting doc or in slack.
* Priority to candidate interviews, E-Group, GitLab BoD, Investor Relations and 1:1's.
* When in-person meetings are being requested, please check with the CFO prior to scheduling the meeting.
* For interviews: CES should tag Sr. EBA on Greenhouse to review times submitted by the candidate, or to provide times for the VP.
* EBA to add reminders at 1 month, 2 weeks, 1 week and 2 days on the calendar for the following events: OKR How to Achieve, Group Conversations and Board of Director meetings.
* A calendar key is available for viewing in the CFO's daily schedule.


## Scheduling for Stella Treas, Chief of Staff

* All meeting requests should go through the CoS's EBA, Katie Gammon
* Priority given to candidate interviews, CEO, E-Group, GitLab BoD, and direct reports to the CoS
* Limited availability due to personal commitments from 7-9am PT Mon.-Fri. due personal commitments during the California shelter in place order (related to Covid-19). Can often make meetings during these blocks, if given advance notice, but strong preference for meeting outside of these times
* If meetings need to happen before 9am PT or after 5pm PT check with EBA
* Do not schedule over DNB (Do Not Book) without reaching out for approval from CoS’s EBA
* For meetings outside of set business hours contact the CoS’s EBA
* During Zoom meetings please do not post information in the chat, add it to the meeting doc or in slack
* EBA to add reminders at 1 month, 2 weeks, 1 week and 2 days on the calendar for the following events: OKR How to Achieve, Group Conversations and Board of Director meetings
* All meeting requests should include an agenda or some sort of reference material
* A calendar key is available for viewing in the CoS's daily schedule


## Scheduling for Eric Johnson, VP of Engineering

* 1:1 Meeting title format: “Person:Eric 1:1” using [this template](https://docs.google.com/document/d/1vWm7-lmpqghoElckd02puqsKDNT6aCJInuZcfkdtvwQ/edit)
* Tick the box “Attendees can modify”
* Please create an additional reminder (besides the default 10 min pop-up) that is 1 day. Email (reminds me to go into the doc and populate notes)
* All meetings should be set using a one-time meeting ID from Eric’s zoom room to avoid any accidental joins.  This includes interviews.  Note: Eric will attach private 1:1 docs to the meeting series once they are created.
* Eric will block off personal appointments and family related blocks.
* Add holds when scheduling meetings and interviews so he knows it's being worked on.
* If meetings need to happen before 8am PT or after 6pm PT check with him directly.
* Slack should be used for instant back and forth communication, not for items requiring followup or action
* Email is the preferred method of contact for any items that need follow up, approvals or action by Eric
* Meeting requests for Eric should be posted in #eba-team  channel on Slack tagging EBA
* [Skip-Level meetings](https://about.gitlab.com/handbook/leadership/skip-levels/#the-purpose-of-skip-level-meetings) are true skip-levels and attendees are the direct reports of the CTO's direct reports
    * Other ways to connect with Eric:
      * Attend the weekly CTO Office Hours
      * Attend the quarterly AMA with Eric
      * Message in the #cto slack channel

#### **Eric's Meetings:**

* Eric is Required (try not to move this meeting)
  * Engineering Staff Meeting
  * E-Group Meeting
  * PM&Eng (with CEO)
  * Skip-levels
  * Engineering Key Meetings
* Eric is Required (these meetings can be moved)
  * 1:1's with direct reports
  * 1:1's with peers
* Eric is Optional (These can be booked over)
  * Working Group Meetings
  * PM Staff Meetings
  * Availability and Performance Refinement
  * Infrastructure Cost Meetings
  * Company Call
  * Group Conversation
  * Peer's Key Reviews
* Eric can book over these occasionally (use judgement)
  * Lunch
  * 7 -8 AM PST


## Scheduling for Todd Barr, Chief Marketing Officer

* 1:1 Meeting title format: "Todd:Person 1:1"
* Color coding for calendar: 1:1's: blue, Reminders: yellow, Internal Meetings: light red, Marketing Specific Meetings: dark blue, Interviews: green, External Meetings: orange
* All 1:1 meetings should be scheduled on Mondays and Tuesdays
* Keep standing daily "blocks" that he has everyday for lunch and at the end of each day
* Block out open slots with "DNB-reach out to shaynes@"
* Create calendar reminders for GC's, AMA's and monthly key reviews two weeks in advance and then a final reminder one week prior
* Block off hour prep time before all 1:1's with CEO
* Keep Friday as open as possible for work time
* If meetings fall after 5pmEST, check with him directly
* Use personal Zoom links for interviews, all other meetings, especially external meetings, should utilize a one-time Zoom link


## Scheduling for Scott Williamson, Chief Product Officer

* All meeting requests should go through the EBA for Product & Engineering
* 1:1's with direct reports should occur weekly unless scheduling conflicts
* Meetings before 8:00am MDT and after 5:00pm MDT need to be confirmed prior to scheduling
* Weekly Product Meeting and Weekly Product Leadership meetings cannot be scheduled over
* Scott will allocate "DNB" blocks for work time based on upcoming priorities
* Green working blocks can be scheduled over
* Red blocks are either personal appointments or working blocks that cannot be compromised
* Reminders at -2 weeks, -1 week and -2 days should be put ont the calendar for the following events: OKR, Group Conversations (Product and Growth), Board of Director meetings, Monthly Key Meetings
* 1:1's should always be rescheduled instead of cancelled unless unavoidable due to OOO/PTO schedules

## Scheduling for Robin Schulman, Chief Legal Officer

* All meeting requests should go through the CLO's Sr. EBA, Robyn Hartough
* Priority to candidate interviews, CEO, E-Group and GitLab BoD
* Unavailable due to Group Conversations, Company Calls and personal commitments from 7-9am PT
* During Zoom meetings please do not post information in the chat, add it to the meeting doc or in slack
* Calendar key is available for viewing in the CLO's daily schedule
* All meeting requests should include an agenda or doc for reference
* Working blocks on the calendar are OK and can be moved to prioritize other meeting requests, team members should check in with the CLO's Sr. EBA to request a meeting using the meeting request requirements as a guideline

## Scheduling for Wendy Barnes, Chief People Officer
* Please schedule all meetings through the People Function's EBA, Trevor Knudsen
* All meeting requests should include an agenda or doc for reference unless it is a Coffee Chat
* Keep meetings to 25 or 50 minutes unless otherwise specified
* Direct Report 1:1's occur weekly
* 1:1 meetings are dark dark green,  coffee chats are light blue, reminders are in yellow, interviews are in dark blue, anything where attendance is essential and Wendy plays a huge part in the meeting should be noted in bright red.
* Do not schedule over DNB: Work Time blocks without consulting the EBA of the People Group.
Scheduling should ideally be kept between 9:00 am and 6:00 pm Pacific Standard time
